package com.example.block8uploaddownloadfiles.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FicheroRepository extends JpaRepository<Fichero,Long> {
    public Fichero findByNombre(String nombre);
}
