package com.example.block8uploaddownloadfiles.model;


import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Table(name = "fichero")
//@Data
@Getter @Setter @AllArgsConstructor @NoArgsConstructor
@Builder
public class Fichero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;

    private LocalDate fechaSubida;

    private String categoria;
}
