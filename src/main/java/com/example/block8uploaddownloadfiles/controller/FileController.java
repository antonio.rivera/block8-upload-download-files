package com.example.block8uploaddownloadfiles.controller;


import com.example.block8uploaddownloadfiles.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;



@Controller
@RequiredArgsConstructor
public class FileController {

    private final FileService fileService;

    /**
     * Subir archivos
     * @param files
     * @param tipo
     * @return
     */
    @PostMapping(value = "/upload/{tipo}")
    public ResponseEntity<String> uploadFiles(@RequestParam("file")MultipartFile files, @PathVariable String tipo){
        try {
            if (tipo.toString().equals(files.getContentType().substring(files.getContentType().indexOf("/") + 1, files.getContentType().length()).toString())){
                fileService.save(files);
                return ResponseEntity.status(HttpStatus.OK).body("Los archivos se han subido correctamente");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No son iguales: "
                        +tipo
                        +" != "
                        +files.getContentType().substring(files.getContentType().indexOf("/")+1,files.getContentType().length()));
            }
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ocurrio un error al subir el archivo");
        }
    }

    /**
     * Cambia ruta de subida
     * @param path
     * @return
     */
    @PutMapping("/setpath")
    public ResponseEntity<String> cambiarRuta(@RequestParam String path){
        try {
            fileService.dondeGuardar(path);
            return ResponseEntity.status(HttpStatus.OK).body("Se ha cambiado la ruta a: "+path);
        } catch (Exception e) {
            throw new RuntimeException("No se ha cambiado la ruta: "+e);
        }
    }

    /**
     * Carga archivo por nombre
     * @param nombre
     * @return
     */
    @GetMapping("/loadByName")
    public ResponseEntity<Resource> loadByName (@RequestParam String nombre){
        try {
            return ResponseEntity.ok(fileService.loadByName(nombre));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Carga archivo por id
     * @param id
     * @return
     */
    @GetMapping("/loadById")
    public ResponseEntity<Resource> loadById (@RequestParam Long id){
        try {
            return ResponseEntity.ok(fileService.loadById(id));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
