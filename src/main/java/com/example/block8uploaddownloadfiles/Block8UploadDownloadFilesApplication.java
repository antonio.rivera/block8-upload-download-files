package com.example.block8uploaddownloadfiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Block8UploadDownloadFilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(Block8UploadDownloadFilesApplication.class, args);
	}

}
