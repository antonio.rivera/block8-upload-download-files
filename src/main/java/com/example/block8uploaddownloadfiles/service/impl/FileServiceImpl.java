package com.example.block8uploaddownloadfiles.service.impl;

import com.example.block8uploaddownloadfiles.model.Fichero;
import com.example.block8uploaddownloadfiles.model.FicheroRepository;
import com.example.block8uploaddownloadfiles.service.FileService;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {


    private final FicheroRepository ficheroRepository;


    private Path rootFolder = Paths.get("uploads");

    @Override
    public Fichero save(MultipartFile file) throws Exception {
        Files.copy(file.getInputStream(), this.rootFolder.resolve(file.getOriginalFilename()));
        Fichero fichero = new Fichero();
        fichero.setNombre(file.getOriginalFilename());
        fichero.setFechaSubida(LocalDate.now());
        fichero.setCategoria(file.getContentType().substring(file.getContentType().indexOf("/")+1,file.getContentType().length()));
        ficheroRepository.save(fichero);
        return fichero;
    }

    @Override
    public Resource loadByName(String name) throws Exception {
        Path file = rootFolder.resolve(name);
        Resource resource = new UrlResource(file.toUri());
        return resource;
    }


    @Override
    public Path dondeGuardar(String path) throws Exception {
        rootFolder=Paths.get(path);
        return rootFolder;
    }

    @Override
    public Resource loadById(Long id) throws Exception {
        Path file = rootFolder.resolve(ficheroRepository.findById(id).get().getNombre());
        Resource resource = new UrlResource(file.toUri());
        return resource;
    }
}
