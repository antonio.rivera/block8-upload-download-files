package com.example.block8uploaddownloadfiles.service;

import com.example.block8uploaddownloadfiles.model.Fichero;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

public interface FileService {
    public Fichero save(MultipartFile file) throws Exception;

    public Resource loadByName(String name) throws Exception;

    public Path dondeGuardar(String path) throws Exception;

    public Resource loadById(Long id) throws Exception;

}
