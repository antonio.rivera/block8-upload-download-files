package com.example.block8uploaddownloadfiles.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class FicheroTest {

    @Test
    void testEquals() {
        Fichero ficheroSimulado = new Fichero(1L,"holaMundo.txt",LocalDate.of(2024,05,14),"plain");
        Fichero ficheroEsperado = new Fichero(1L,"holaMundo.txt",LocalDate.of(2024,05,14),"plain");
        assertEquals(ficheroEsperado.getId(),ficheroSimulado.getId());
        assertEquals(ficheroEsperado.getNombre(),ficheroSimulado.getNombre());
        assertEquals(ficheroEsperado.getCategoria(),ficheroSimulado.getCategoria());
        assertEquals(ficheroEsperado.getFechaSubida(),ficheroSimulado.getFechaSubida());
    }
    @Test
    void testNotEquals() {
        Fichero ficheroSimulado = new Fichero(2L,"holaMundotxt",LocalDate.of(2014,05,14),"pain");
        Fichero ficheroEsperado = new Fichero(1L,"holaMundo.txt",LocalDate.of(2024,05,14),"plain");
        assertNotEquals(ficheroEsperado.getId(),ficheroSimulado.getId());
        assertNotEquals(ficheroEsperado.getNombre(),ficheroSimulado.getNombre());
        assertNotEquals(ficheroEsperado.getCategoria(),ficheroSimulado.getCategoria());
        assertNotEquals(ficheroEsperado.getFechaSubida(),ficheroSimulado.getFechaSubida());
    }

    /*@Test
    void canEqual() {
        Fichero ficheroSimulado = new Fichero(1L,"holaMundo.txt",LocalDate.of(2024,05,14),"plain");
        Fichero ficheroEsperado = new Fichero(1L,"holaMundo.txt",LocalDate.of(2024,05,14),"plain");
        assertTrue(ficheroSimulado.canEqual(ficheroEsperado));
    }*/


    @Test
    void testHashCode() {
        Fichero ficheroSimulado = new Fichero(1L,"holaMundo.txt",LocalDate.of(2024,05,14),"plain");
        Fichero ficheroEsperado = new Fichero(1L,"holaMundo.txt",LocalDate.of(2024,05,14),"plain");
        assertEquals(ficheroSimulado.getId().hashCode(),ficheroEsperado.getId().hashCode());
        assertEquals(ficheroSimulado.getFechaSubida().hashCode(),ficheroEsperado.getFechaSubida().hashCode());
        assertEquals(ficheroSimulado.getNombre().hashCode(),ficheroEsperado.getNombre().hashCode());
        assertEquals(ficheroSimulado.getCategoria().hashCode(),ficheroEsperado.getCategoria().hashCode());
    }
    @Test
    void testHashNoCode() {
        Fichero ficheroSimulado = new Fichero(2L,"holaMundotxt",LocalDate.of(2014,05,14),"pain");
        Fichero ficheroEsperado = new Fichero(1L,"holaMundo.txt",LocalDate.of(2024,05,14),"plain");

        assertNotEquals(ficheroSimulado.getId().hashCode(),ficheroEsperado.getId().hashCode());
        assertNotEquals(ficheroSimulado.getFechaSubida().hashCode(),ficheroEsperado.getFechaSubida().hashCode());
        assertNotEquals(ficheroSimulado.getNombre().hashCode(),ficheroEsperado.getNombre().hashCode());
        assertNotEquals(ficheroSimulado.getCategoria().hashCode(),ficheroEsperado.getCategoria().hashCode());
    }



    @Test
    void getSetId() {
        Fichero fichero = new Fichero();
        fichero.setId(1L);
        assertEquals(1L,fichero.getId());
    }

    @Test
    void getSetNombre() {
        Fichero fichero = new Fichero();
        fichero.setId(1L);
        assertEquals(1L,fichero.getId());
    }

    @Test
    void getSetFechaSubida() {
        Fichero fichero = new Fichero();
        fichero.setFechaSubida(LocalDate.now());
        assertEquals(LocalDate.now(),fichero.getFechaSubida());
    }

    @Test
    void getSetCategoria() {
        Fichero fichero = new Fichero();
        fichero.setCategoria("plain");
        assertEquals("plain",fichero.getCategoria());
    }

    @Test
    void NogetSetId() {
        Fichero fichero = new Fichero();
        fichero.setId(2L);
        assertNotEquals(1L,fichero.getId());
    }

    @Test
    void NogetSetNombre() {
        Fichero fichero = new Fichero();
        fichero.setId(1L);
        assertNotEquals(2L,fichero.getId());
    }

    @Test
    void NogetSetFechaSubida() {
        Fichero fichero = new Fichero();
        fichero.setFechaSubida(LocalDate.now());
        assertNotEquals(LocalDate.of(2000,12,01),fichero.getFechaSubida());
    }

    @Test
    void NogetSetCategoria() {
        Fichero fichero = new Fichero();
        fichero.setCategoria("plin");
        assertNotEquals("plain",fichero.getCategoria());
    }


    @Test
    void builder() {
        Fichero fichero = Fichero.builder()
                .id(1L)
                .nombre("holaMundo.txt")
                .fechaSubida(LocalDate.now())
                .categoria("plain")
                .build();
        assertEquals(1L,fichero.getId());
        assertEquals("holaMundo.txt",fichero.getNombre());
        assertEquals(LocalDate.now(),fichero.getFechaSubida());
        assertEquals("plain",fichero.getCategoria());
    }
    @Test
    void noBuilder() {
        Fichero fichero = Fichero.builder()
                .id(2L)
                .nombre("holaundo.txt")
                .fechaSubida(LocalDate.of(2000,12,01))
                .categoria("plan")
                .build();
        assertNotEquals(1L,fichero.getId());
        assertNotEquals("holaMundo.txt",fichero.getNombre());
        assertNotEquals(LocalDate.now(),fichero.getFechaSubida());
        assertNotEquals("plain",fichero.getCategoria());
    }


}