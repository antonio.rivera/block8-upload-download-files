package com.example.block8uploaddownloadfiles.service.impl;

import com.example.block8uploaddownloadfiles.model.Fichero;
import com.example.block8uploaddownloadfiles.model.FicheroRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;


import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
class FileServiceImplTest {

    @Mock
    private FicheroRepository ficheroRepository;

    @InjectMocks
    private FileServiceImpl fileServiceImpl;

    @Test
    void save() throws Exception {
        if (Files.exists(Path.of("uploads/holaMundo.txt"))){
            Files.delete(Path.of("uploads/holaMundo.txt"));
        }
        MultipartFile file = new MockMultipartFile("name","holaMundo.txt", MediaType.TEXT_PLAIN_VALUE,"Hola mundo".getBytes());

        Fichero ficheroSimulado = new Fichero(1L,"holaMundo.txt",LocalDate.of(2024,05,14),"plain");
        Fichero ficheroEsperado=fileServiceImpl.save(file);
        ficheroEsperado.setId(1L);
        assertEquals(ficheroEsperado.getId(),ficheroSimulado.getId());
        assertEquals(ficheroEsperado.getNombre(),ficheroSimulado.getNombre());
        assertEquals(ficheroEsperado.getCategoria(),ficheroSimulado.getCategoria());
        assertEquals(ficheroEsperado.getFechaSubida(),ficheroSimulado.getFechaSubida());

    }

    @Test
    void loadByName() throws Exception {
        if (Files.exists(Path.of("uploads/holaMundo.txt"))){
            Files.delete(Path.of("uploads/holaMundo.txt"));
        }

        MultipartFile file = new MockMultipartFile("name","holaMundo.txt", MediaType.TEXT_PLAIN_VALUE,"Hola mundo".getBytes());
        Fichero ficheroEsperado=fileServiceImpl.save(file);
        Resource resource = fileServiceImpl.loadByName("holaMundo.txt");
        assertEquals(resource.getFilename(),ficheroEsperado.getNombre());
    }


    @Test
    void dondeGuardar() throws Exception {
        if (Files.exists(Path.of("uploads/holaMundo.txt"))){
            Files.delete(Path.of("uploads/holaMundo.txt"));
        }
        Path rootFolder = Paths.get("uploads");
        Path rootFolderEsperado = fileServiceImpl.dondeGuardar("uploads");
        assertEquals(rootFolder,rootFolderEsperado);
    }

    @Test
    void loadById() throws Exception {
        if (Files.exists(Path.of("uploads/holaMundo.txt"))){
            Files.delete(Path.of("uploads/holaMundo.txt"));
        }
        Fichero ficheroSimulado = new Fichero(1L,"holaMundo.txt",LocalDate.of(2024,05,14),"plain");
        MultipartFile file = new MockMultipartFile("name","holaMundo.txt", MediaType.TEXT_PLAIN_VALUE,"Hola mundo".getBytes());
        Mockito.when(ficheroRepository.findById(1L)).thenReturn(Optional.of(ficheroSimulado));
        Resource resource=fileServiceImpl.loadById(1L);
        assertEquals(resource.getFilename(),file.getOriginalFilename());
    }
}