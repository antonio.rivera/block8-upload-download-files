package com.example.block8uploaddownloadfiles.controller;

import com.example.block8uploaddownloadfiles.model.Fichero;
import com.example.block8uploaddownloadfiles.service.impl.FileServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;


import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.nio.file.Path;
import java.time.LocalDate;




@SpringBootTest
@WebAppConfiguration
@AutoConfigureMockMvc
class FileControllerTest {

    @Autowired
    private MockMvc mockMvc;

    //@Autowired
    //private WebApplicationContext webApplicationContext;

    @MockBean
    private FileServiceImpl fileService;

    private Fichero fichero;

    //private Path rootFolder=Paths.get("uploads");

    @BeforeEach
    void setUp() {
        //Path rootFolder = Paths.get("uploads");
         fichero = Fichero.builder()
                .id(1L)
                .nombre("holaMundo.txt")
                .fechaSubida(LocalDate.of(2024,05,13))
                .categoria("plain")
                .build();
       // mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void uploadFiles() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file","holaMundo.txt", "text/plain","Hola mundo".getBytes());
        Mockito.when(fileService.save(file)).thenReturn(fichero);
        mockMvc.perform(MockMvcRequestBuilders.multipart("/upload/plain").file(file))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Los archivos se han subido correctamente"));

    }
    @Test
    void uploadFilesNoIguales() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file","holaMundo.txt", "text/plain","Hola mundo".getBytes());
        Mockito.when(fileService.save(file)).thenReturn(fichero);
        mockMvc.perform(MockMvcRequestBuilders.multipart("/upload/textu").file(file))
                .andExpect(MockMvcResultMatchers.status().is(500))
                .andExpect(MockMvcResultMatchers.content().string("No son iguales: textu != plain"));
    }


    @Test
    void cambiarRuta() throws Exception {

        Mockito.when(fileService.dondeGuardar("uploads")).thenReturn(Path.of("uploads"));
        mockMvc
                .perform(MockMvcRequestBuilders.put("/setpath").queryParam("path","uploads"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Se ha cambiado la ruta a: uploads"));
    }
    @Test
    void noCambiarRuta() throws Exception {

        Mockito.when(fileService.dondeGuardar("upload")).thenReturn(Path.of("upload"));

        Path rootFolder=fileService.dondeGuardar("upload");
        mockMvc
                .perform(MockMvcRequestBuilders.put("/setpath").param("path",rootFolder.toString()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Se ha cambiado la ruta a: upload"));
    }


    @Test
    void loadByName() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/loadByName")
                        .param("nombre","gsweeswdg"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void loadByIdError() throws Exception {
        //MockMultipartFile file = new MockMultipartFile("file","holaMundo.txt", "text/plain","Hola mundo".getBytes());
        //Mockito.when().thenReturn(fichero);
        //Mockito.when(fileService.loadById(1L)).thenReturn();
        //assertEquals(resource.getFilename(),file.getOriginalFilename());
         mockMvc.perform(MockMvcRequestBuilders.get("/loadById")
                        .param("id","1L"))
                        .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}